LABEL
xlabel('n');
ylabel('x1[n]');
ax = [min(n1), max(n1), min(x1), max(x1)];
axis(ax);
title('1.A.1');

MAKE X PERIODIC
xPeriodic = x' * ones(1, 25);
xPeriodic = xPeriodic(:);

FUNCTION
function [n, x] = sinusoid(A, w0, phi, nStart, nEnd)

LINSPACE
t = linspace(tStart, tEnd, 1e4);

HOLDON
figure();
plot(t, xC);
hold on;
stem(n/8000, xD);
hold off;

IMPULSE RESPONSE
[h, n] = impz(b,a, 100);

ZPLANE
zplane(b, a);

STEP RESPONSE
[sR, n] = stepz(b,a, 200);

FILTER
yn = filter(b, a, xn);

DTFT
w = linspace(-pi, pi, 2000);
X = xn * exp(-j*(n')*w);
figure();
subplot(2,1, 1);
plot(w/pi, real(X));
xlabel('w/pi');
ylabel('real(X)');
title('5.A.2');
subplot(2,1, 2);
plot(w/pi, imag(X));
xlabel('w/pi');
ylabel('imag(X)');
title('5.A.2');

UNWRAP
angleUW = unwrap(angleX);

FREQUENCY RESPONSE
[H, w] = freqz(b, a);

GROUPDELAY
[Gd, w] = grpdelay(b, a);

SINC RECONSTRUCTION
xrs = xs * sinc((1/Ts) * ( (ones(length(n), 1) )* t - ((n*Ts)') *ones(1, length(t))));
figure(7);
plot(t, xrs);

ZEROPHASE
[H, w, Phi] = zerophase(b, a);

DFS
N = 10;
n = [0:1:N-1];
x = ones(1,N-1);
k = [0:1:N-1]; 
W = exp(-j*2*pi/N);
Wk = W .^ k; 
Xk = x * Wk; 

IDFS
N = 10;
n = [0:1:N-1];
k = [0:1:N-1]; 
W = exp(j*2*pi/N);
Wk = W .^ k; 
x = Xk * Wk; 