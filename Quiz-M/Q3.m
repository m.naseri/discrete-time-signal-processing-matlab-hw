% M.Mehdi Naseri 9423115 DSP M %
close all;
clear all;
clc;

alpha = 0.1;
L = 10;
n = -L:L;
g = exp(-alpha*(n.^2));
figure();
stem(n, g);


gPrime = [g(L+1:2*L+1) g(1:L)];
figure();
stem(gPrime);
N = 2*L + 1;
G = fft(gPrime, N);

figure();
subplot(2, 1, 1);
plot(real(G));
subplot(2, 1, 2);
plot(imag(G));

figure();
GSH = fftshift(real(G));
plot(GSH);
