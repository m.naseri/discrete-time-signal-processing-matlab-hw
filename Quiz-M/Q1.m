% M.Mehdi Naseri 9423115 DSP M %
close all;
clear all;
clc;

[xn, Fs] = audioread('speech_dft.wav');
h = ones(1, 512);
N = 512;
tic;
y1 = overlapAdd(xn, h, N);
t1 = toc; % --> 1.0098s
figure();
stem(y1);

tic;
y2 = conv(xn, h);
t2 = toc; % --> 0.0120s
figure();
stem(y2);