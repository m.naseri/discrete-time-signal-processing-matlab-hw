 function [y] = overlapSave(x, h, Nfft)
 H = fft(h, Nfft);
 L = length(h);
 M = Nfft - L + 1;
 ix = 0;
 while ix+Nfft <= length(x)
     X = fft(x(1+ix : Nfft+ix), Nfft );
     Y = X .* H;
     yTemp = ifft(Y, Nfft);
     y(1+ix : M+ix) = yTemp(L:Nfft);    
     ix = ix + M;
 end