% M.Mehdi Naseri 9423115 DSP M %
close all;
clear all;
clc;

[xn, Fs] = audioread('speech_dft.wav');
h = kaiser(1024, 2.86);

N = 1024;

tic;
y1 = overlapSave(xn, h, N);
t1 = toc; % --> 2.2015s
figure();
stem(y1);

tic;
y2 = conv(xn, h);
t2 = toc; % --> 0.0146s
figure();
stem(y2);