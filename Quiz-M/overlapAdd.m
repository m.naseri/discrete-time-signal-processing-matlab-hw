function [y] = overlapAdd(x, h, Nfft)
H = fft(h, Nfft);
M = Nfft - length(h) + 1;
y = zeros(1, length(x)+Nfft-1);
for ix = 1:M:length(x)
    x_seg = x(ix:ix+M-1);
    X = fft(x_seg, Nfft);
    Y = X .* H;
    y_seg = ifft(Y);
    y(ix:ix+Nfft-1) = y(ix:ix+Nfft-1) + y_seg(1:Nfft);
end

if ~any(imag(h)) & ~any(imag(x))
    y = real(y);
end