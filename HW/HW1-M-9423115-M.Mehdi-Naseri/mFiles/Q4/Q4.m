% M.Mehdi Naseri 9423115 DSP M HW1 %
% Q4
close all;
clear all;
clc;

% a
b = [0.3 0.6 0.3];
a = [1 0 0.9];
aRoots = roots(a); %    0.0000 +- 0.9487i
n = 0:20;
xn = (0.0000 + 0.9487i).^(n);

figure();

subplot(2, 1, 1);
stem(n, real(xn));
xlabel('n');
ylabel('real(x[n])');

subplot(2, 1, 2);
stem(n, imag(xn));
xlabel('n');
ylabel('imag(x[n])');

n = 0:20;
xn = (0.0000 - 0.9487i).^(n);

figure();

subplot(2, 1, 1);
stem(n, real(xn));
xlabel('n');
ylabel('real(x[n])');

subplot(2, 1, 2);
stem(n, imag(xn));
xlabel('n');
ylabel('imag(x[n])');

[h, n] = impz(b,a, 100);
figure();
stem(n, h);
xlabel('n');
ylabel('h[n]');
title('3.A');

sumOfH = sum(abs(h)); % ---> 6.6 STABLE
zplane(b, a);

% b
[sR, n] = stepz(b,a, 200); % *3----> G: 1.8947
stem(n, 3*sR);
xlabel('n');
ylabel('3*stepResponse[n]');
title('3.B');

% c
% G: 1.8947

% d
n = -1000:1000;
xn = 3*cos(n*pi/10);
yn = filter(b, a, xn);
n = n(950:1050);
yn = yn(950:1050);
stem(n, yn);
xlabel('n');
ylabel('yn');
title('3.D');

% e
% FROM A
[h, n] = impz(b,a, 100);
figure();
stem(n, h);
xlabel('n');
ylabel('h[n]');
title('3.E.a'); % like before (G = 0)
%%%%%%%%
% FROM C
[sR, n] = stepz(b,a, 200); % *3----> G: 1.8947
stem(n, 3*sR - 1.8947);
xlabel('n');
ylabel('3*stepResponseT[n]');
title('3.E.c');

% f
[h, n] = impz(b,a, 200);
diffOfh = diff(h);
n = 1:199;
figure();
subplot(2, 1, 1);
stem(n, diffOfh);
xlabel('n');
ylabel('diffOfh[n]');
title('3.F');

subplot(2, 1, 2);
n = 0:199;
stem(n, sR-1.8947/3);
xlabel('n');
ylabel('stepResponse[n]');
title('3.F');
