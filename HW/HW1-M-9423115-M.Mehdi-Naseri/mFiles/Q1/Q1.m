% M.Mehdi Naseri 9423115 DSP M HW1 %
% Q1
close all;
clear all;
clc;

% a1
n1 = 1:20;
x1 = [0.9 * (n1 == 5)];
figure();
stem(n1, x1);
xlabel('n');
ylabel('x1[n]');
ax = [min(n1), max(n1), min(x1), max(x1)];
axis(ax);
title('1.A.1');
saveas(gcf, '1.A.1.png');

% a2
n2 = -21:15;
x2 = [0.8 * (n2 == 3)];
figure();
stem(n2, x2);
xlabel('n');
ylabel('x2[n]');
ax = [min(n2), max(n2), min(x2), max(x2)];
axis(ax);
title('1.A.2');
saveas(gcf, '1.A.2.png');

% a3
n3 = -16:0;
x3 = [1.5 * (n3 == -7)];
figure();
stem(n3, x3);
xlabel('n');
ylabel('x3[n]');
ax = [min(n3), max(n3), min(x3), max(x3)];
axis(ax);
title('1.A.3');
saveas(gcf, '1.A.3.png');

% b
% P = 50, M*P = 50 -> M = 10
n = 0:45; % you can replace with 50 if
          % the upperband of sigma goes to M (not M-1)
s = [mod(n, 5) == 0];
figure();
stem(n, s);
xlabel('n');
ylabel('s[n]');
title('1.B');
saveas(gcf, '1.B.1.png');

% c
n = -7:7;
x = [(n>=-5 & n<=5)];
figure();
stem(n, x);
xlabel('n');
ylabel('x[n]');
title('1.C rectangular');
saveas(gcf, '1.C rectangular.png');
temp = length(n)*25-1;
n = -temp/2:temp/2;
xPeriodic = x' * ones(1, 25);
xPeriodic = xPeriodic(:);
figure();
stem(n, xPeriodic);
xlabel('n');
ylabel('xPeriodic[n]');
title('1.C periodicRectangular');
saveas(gcf, '1.C xPeriodic[n].png');