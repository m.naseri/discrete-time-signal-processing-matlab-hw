% M.Mehdi Naseri 9423115 DSP M HW1 %
% Q6
close all;
clear all;
clc;

% a
n = -60:60;
xn = [(n >=0) & (n <= 19)];
w = linspace(-pi, pi, 1e6);
X = xn * exp(-j*(n')*w);
figure();
stem(n, xn);
xlabel('n');
ylabel('x[n]');
title('6.A.1');

figure();
subplot(2, 1, 1);
plot(w/pi, phase(X)/pi);
xlabel('w/pi');
ylabel('phase(X)/pi');
title('6.A.2 PHASE');

subplot(2, 1, 2);
plot(w/pi, angle(X)/pi);
xlabel('w/pi');
ylabel('angle(X)/pi');
title('6.A.3 ANGLE');

% b
figure();
angleX = angle(X);
angleUW = unwrap(angleX);
plot(w/pi, angleUW/pi);
xlabel('w/pi');
ylabel('unwrappedAngle(X)/pi');
title('6.B FAILEDUnwrapped Phase');