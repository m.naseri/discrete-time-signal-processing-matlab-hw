function [n, yn] = z0toTheNFilterVersion(r, theta, nStart, nEnd)
n = nStart:nEnd;
xn = [n == nStart];
b = [1];
a = [1 -r*exp(j*theta)];
yn = filter(a, b, xn);