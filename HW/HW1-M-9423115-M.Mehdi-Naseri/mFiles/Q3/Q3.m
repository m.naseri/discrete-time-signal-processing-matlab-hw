% M.Mehdi Naseri 9423115 DSP M HW1 %
% Q3
close all;
clear all;
clc;

% a
[n, xn1] = z0toTheN(1, pi/7, -10, 10);
[n, xn2] = z0toTheN(1, pi/7, -10, 10);

xn = 3*(xn1 - xn2)/(2j) + 4*(xn1 + xn2)*j/2;

figure();
title('3.A');

subplot(2, 2, 1);
stem(n, real(xn));
xlabel('n');
ylabel('real(x[n])');

subplot(2, 2, 2);
stem(n, imag(xn));
xlabel('n');
ylabel('img(x[n])');

subplot(2, 2, 3);
stem(n, abs(xn));
xlabel('n');
ylabel('abs(x[n])');

subplot(2, 2, 4);
stem(n, angle(xn));
xlabel('n');
ylabel('angle(x[n])');

% b
[n, xn1] = z0toTheNFilterVersion(1, pi/7, -10, 10);
[n, xn2] = z0toTheNFilterVersion(1, pi/7, -10, 10);

xn = 3*(xn1 - xn2)/(2j) + 4*(xn1 + xn2)*j/2;

figure();
title('3.B');

subplot(2, 2, 1);
stem(n, real(xn));
xlabel('n');
ylabel('real(x[n])');

subplot(2, 2, 2);
stem(n, imag(xn));
xlabel('n');
ylabel('img(x[n])');

subplot(2, 2, 3);
stem(n, abs(xn));
xlabel('n');
ylabel('abs(x[n])');

subplot(2, 2, 4);
stem(n, angle(xn));
xlabel('n');
ylabel('angle(x[n])');
