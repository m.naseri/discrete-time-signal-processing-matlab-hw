function [X] = asinc(L, w)
X = sin(1/2 * w * L) ./ sin(1/2 * w);