% M.Mehdi Naseri 9423115 DSP M HW1 %
% Q5
close all;
clear all;
clc;

% a, b
n = -60:60;
xn = [(n >=0) & (n <= 12)];
figure();
stem(n, xn);
xlabel('n');
ylabel('x[n]');
title('5.A.1');

w = linspace(-pi, pi, 2000);
X = xn * exp(-j*(n')*w);
figure();
subplot(2,1, 1);
plot(w/pi, real(X));
xlabel('w/pi');
ylabel('real(X)');
title('5.A.2');
subplot(2,1, 2);
plot(w/pi, imag(X));
xlabel('w/pi');
ylabel('imag(X)');
title('5.A.2');

figure();
subplot(2,1, 1);
plot(w/pi, abs(X));
xlabel('w/pi');
ylabel('abs(X)');
title('5.A.3');
subplot(2,1, 2);
plot(w/pi, angle(X)/pi);
xlabel('w/pi');
ylabel('angle(X)/pi');
title('5.A.3');

% c
n = -60:60;
xn = [(n >=0) & (n <= 15)];
figure();
stem(n, xn);
xlabel('n');
ylabel('x[n]');
title('5.C.1');

w = linspace(-pi, pi, 2000);
X = xn * exp(-j*(n')*w);
figure();
subplot(2,1, 1);
plot(w/pi, real(X));
xlabel('w/pi');
ylabel('real(X)');
title('5.C.2');
subplot(2,1, 2);
plot(w/pi, imag(X));
xlabel('w/pi');
ylabel('imag(X)');
title('5.C.2');

figure();
subplot(2,1, 1);
plot(w/pi, abs(X));
xlabel('w/pi');
ylabel('abs(X)');
title('5.C.3');
subplot(2,1, 2);
plot(w/pi, angle(X)/pi);
xlabel('w/pi');
ylabel('angle(X)/pi');
title('5.C.3');

% --> Number of zeros is associated with L (2pi*k/L)
% d -> There is no D part in questions.
% e




% This (abs(X))is same as part b 
