function [t, xC, n, xD] = sampledSinusoid(A, f0, phi, tStart, tEnd, fs)
t = linspace(tStart, tEnd, 1e4);
xC = A * cos(2*pi*f0*t + phi);
Ts = 1/fs;

n = (tStart/Ts):(tEnd/Ts);
xD = A * cos(2*pi*f0*n*Ts + phi);