function [n, x] = sinusoid(A, w0, phi, nStart, nEnd)
n = nStart:nEnd;
x = A * cos(w0*n + phi);
x = x'; % column vector