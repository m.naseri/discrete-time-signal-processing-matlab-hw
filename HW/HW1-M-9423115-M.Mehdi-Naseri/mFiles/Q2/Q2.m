% M.Mehdi Naseri 9423115 DSP M HW1 %
% Q2
close all;
clc;

% a
[n, x] = sinusoid(1, pi/2, 0, 0, 10);
figure();
stem(n, x);
xlabel('n');
ylabel('cos(npi/2)');
title('2.A.1');
saveas(gcf, '2.A.1.png');

[n, x] = sinusoid(1, pi/4, -pi/4, -1, 10);
figure();
stem(n, x);
xlabel('n');
ylabel('cos(npi/4 - pi/4)');
title('2.A.2');
saveas(gcf, '2.A.2.png');

% 2sin(npi/11) for -20 <= n <= 20
[n, x] = sinusoid(2, pi/11, -pi/2, -20, 20);
figure();
stem(n, x);
xlabel('n');
ylabel('2sin(npi/11)');
title('2.A');
saveas(gcf, '2.A.png');

% b
[t, xC, n, xD] = sampledSinusoid(50, 1200, pi/4, 0, 0.007, 8000);
figure();
plot(t, xC);
hold on;
stem(n/8000, xD);
xlabel('t (s)');
ylabel('Sinusoid (50cos(2*pi*1200t + pi/4))');
title('2.B');
hold off;
