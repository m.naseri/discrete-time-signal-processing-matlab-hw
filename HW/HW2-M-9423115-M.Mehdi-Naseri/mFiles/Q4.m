% M.Mehdi Naseri 9423115 DSP M HW2 %
% Q4
close all;
clear all;
clc;

% a
load('gdeldata.mat');
hIIR = impz(b, a, 150);
figure();
stem(hIIR);
title('hIIR');

% b
[HIIR, w] = freqz(b, a);
figure();
subplot(2, 1, 1);
plot(w, abs(HIIR));
title('abs(HIIR)');
[GdIIR, w] = grpdelay(b, a);
subplot(2, 1, 2);
plot(w, GdIIR);
title('GdIIR');

% c
figure();
subplot(2, 1, 1);
stem(x1);
title('x1');
subplot(2, 1, 2);
stem(x2);
title('x2');

figure();
N = 256;
k = 0:N-1;
w = 2*pi*k/N;
X1 = fft(x1);
subplot(2, 1, 1);
plot(w/pi, abs(X1));
title('abs(X1)');
subplot(2, 1, 2);
plot(w/pi, angle(X1));
title('angle(X1)');

figure();
X2 = fft(x2);
subplot(2, 1, 1);
plot(w/pi, abs(X2));
title('abs(X2)');
subplot(2, 1, 2);
plot(w/pi, angle(X2)/pi);
title('angle(X2)');

% d
y1 = filter(b, a, x1);
y2 = filter(b, a, x2);
figure();
subplot(2, 1, 1);
stem(y1);
title('y1IIR');
subplot(2, 1, 2);
stem(y2);
title('y2IIR');
