% M.Mehdi Naseri 9423115 DSP M HW2 %
% Q6
close all;
clear all;
clc;

load('gdeldata.mat');
ypIIR = filter(b, a, pulse);
ypFIR = filter(h, 1, pulse);
figure();
stem(ypIIR);
title('ypIIR');
figure();
stem(ypFIR);
title('ypFIR');
