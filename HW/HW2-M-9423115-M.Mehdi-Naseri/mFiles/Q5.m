% M.Mehdi Naseri 9423115 DSP M HW2 %
% Q5
close all;
clear all;
clc;

% a
load('gdeldata.mat');
figure();
stem(h);
title('hFIR');

[HFIR, w] = freqz(h);
figure();
subplot(2, 1, 1);
plot(w, abs(HFIR));
title('HFIR');

[GdFIR, w] = grpdelay(h);
subplot(2, 1, 2);
plot(w, GdFIR);
title('GdFIR');

% b
y1 = filter(h, 1, x1);
y2 = filter(h, 1, x2);
figure();
subplot(2, 1, 1);
stem(y1);
title('y1FIR');
subplot(2, 1, 2);
stem(y2);
title('y2FIR');