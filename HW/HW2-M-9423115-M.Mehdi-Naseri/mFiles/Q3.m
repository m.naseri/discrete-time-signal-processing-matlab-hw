% M.Mehdi Naseri 9423115 DSP M HW2 %
% Q3
close all;
clear all;
clc;

% a
p1 = [1 0 2 0 4 0 4 0 2 0 1];
r1 = roots(p1);
    % r:
    %   -0.7706 + 0.9723i
    %   -0.7706 - 0.9723i
    %    0.7706 + 0.9723i
    %    0.7706 - 0.9723i
    %   -0.5006 + 0.6317i
    %   -0.5006 - 0.6317i
    %    0.0000 + 1.0000i
    %    0.0000 - 1.0000i
    %    0.5006 + 0.6317i
    %    0.5006 - 0.6317i

p2 = [3 2 -4 1 -1 -3];
r2 = roots(p2);

    % r:
    %   -1.5719 + 0.0000i
    %    1.1224 + 0.0000i
    %    0.2714 + 0.8199i
    %    0.2714 - 0.8199i
    %   -0.7600 + 0.0000i
r2ABS = abs(r2);

% b
h1 = p1;

grp1 = myGrpDelayNstart(h1, 11, 0);
figure();
plot(grp1);

h2 = p2;
grp2 = myGrpDelayNstart(h2, 6, 0);
figure();
plot(grp1);


h3 = [3 2 1 3 3];
grp3 = myGrpDelayNstart(h3, 5, 0);
figure();
plot(grp1);
