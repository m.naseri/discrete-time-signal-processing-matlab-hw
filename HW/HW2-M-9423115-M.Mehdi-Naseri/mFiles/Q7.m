% M.Mehdi Naseri 9423115 DSP M HW2 %
% Q7
close all;
clear all;
clc;

load('gdeldata.mat');
ypnd1IIR = filter(b, a, pnd_1);
ypnd2IIR = filter(b, a, pnd_2);
figure();
subplot(2, 1, 1);
title('y_pnd_1 IIR');
stem(ypnd1IIR);
subplot(2, 1, 2);
stem(ypnd2IIR);
title('y_pnd_2 IIR');

ypnd1FIR = filter(h, 1, pnd_1);
ypnd2FIR = filter(h, 1, pnd_2);
figure();
subplot(2, 1, 1);
stem(ypnd1FIR);
title('y_pnd_1 FIR');
subplot(2, 1, 2);
stem(ypnd2FIR);
title('y_pnd_2 FIR');
