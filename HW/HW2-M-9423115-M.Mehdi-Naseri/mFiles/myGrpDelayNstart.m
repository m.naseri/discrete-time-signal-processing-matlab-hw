function gD = myGrpDelayNstart(h, N, nstart)
n = (nstart) : (nstart+N-1);
num = fft(n .* h, N);
den = fft(h, N);
result = zeros(1, length(den));
result(den ~= 0) = num(den ~= 0) ./ den(den ~= 0);
result(den == 0) = inf;

gD = real(result);