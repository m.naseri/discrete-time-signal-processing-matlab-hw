% M.Mehdi Naseri 9423115 DSP M HW2 %
% Q2
close all;
clear all;
clc;

% b
h = [0, 0, 0, 0, 1];
grpDelay = myGrpDelay(h, 5);
figure();
plot(grpDelay);

% c
grpDelay = myGrpDelayNstart(h, 5, 0);
figure();
plot(grpDelay);